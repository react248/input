function Input() {
  function onInputChange(Event) {
    console.log(Event.target.value);
  }
  return <input type="text" onChange={onInputChange}></input>;
}

export default Input;
